const genConfig = (testRegex) => ({
  globals: {
    'ts-jest': {
      diagnostics: { pathRegex: testRegex },
    },
  },
  testRegex,
  collectCoverageFrom: ['**/src/**'],
  testPathIgnorePatterns: ['/node_modules/', '/ts-out/', '/lib/', 'dist', 'preprocessed'],
  moduleDirectories: ['node_modules'],
  transform: { '^.+\\.tsx?$': 'ts-jest' },
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],
})

let config

if (process.env.WD_TEST) {
  config = genConfig('integration-tests/.*\\.[jt]sx?$')
  config.preset = 'jest-puppeteer'
  config.setupTestFrameworkScriptFile = './jest-puppeteer-setup.js'
} else {
  config = genConfig('code/.*(/__tests__/.*|(\\.|/)(spec|test))\\.[jt]sx?$')
}

module.exports = config
