# Identify Problematic Extension

This is a web extension meant to assist the user in identifying which of their other installed extensions is causing an issue on the current page. 

This extension assists by disabling a subset of the user's extensions and testing if the issue persists. Currently the user may do this in two ways

1. The user may enter a javascript statement to be executed as a direct test of whether the issue persists. E.g. to check if a certain element exists on the page
2. The user can be asked whether the issue is still present.


# Installation
[Chrome Webstore](https://chrome.google.com/webstore/detail/identify-problematic-exte/bdgepmafgjgfneebioinggcpmjidmmfa)
