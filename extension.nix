{ lib, stdenv, nodejs_22 }:

stdenv.mkDerivation {
  pname = "your-extension-name";
  version = "1.0.0"; # Update this as needed

  src = ./.; # Assuming extension files are in the same directory

  nativeBuildInputs = [ nodejs_22 ];

  buildPhase = ''
    ${nodejs_22}/bin/npx crx pack ./src
    # If you have a key file, you can sign the extension like this:
    # chrome-web-store-upload-cli sign --source extension.zip --key path/to/your/key.pem
  '';

  installPhase = ''
    mkdir -p $out
    cp extension.zip $out/
  '';

  meta = with lib; {
    description = "Your Chrome Extension";
    license = licenses.mit; # Adjust as needed
  };
}
