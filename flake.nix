{
  description = "Chrome Extension Builder";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        buildExtension = pkgs.callPackage ./extension.nix { };
      in {
        devShells.default = pkgs.mkShell {
          buildInputs = with pkgs; [ nodejs nodePackages.npm chromium ];

          shellHook = ''
            echo "Chrome Extension Development Environment"
            npm install
            npm run dev
          '';
        };

        packages = {
          extension = buildExtension;
          default = buildExtension;
        };
      });
}
