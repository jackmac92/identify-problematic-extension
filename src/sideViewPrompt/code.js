chrome.runtime.onMessage.addListener((request, sender, respond) => {
  console.debug('Received runtime message!')
  document.querySelector('#message-display').innerText = request.message
  document.querySelector('#details').innerText = request.payload || ''
  try {
    document.querySelector(`#${request.type}`).style.display = 'block'
  } catch (err) {
    console.warn('Unable to make display visible')
  }
  new Promise((resolve) => {
    console.debug('Setting binary responder')
    const cancelButton = document.querySelector('#cancel')
    cancelButton.addEventListener('click', () => sendResponse(null))
    const affirmButton = document.querySelector('#yes')
    affirmButton.addEventListener('click', () => sendResponse(true))
    const rejectButton = document.querySelector('#no')
    rejectButton.addEventListener('click', () => sendResponse(false))
    affirmButton.disabled = false
    rejectButton.disabled = false
    function sendResponse(value) {
      affirmButton.disabled = true
      rejectButton.disabled = true
      resolve({ value, reqId: request.reqId })
    }
  }).then((result) => {
    respond(result)
  })
  return true // NOTE important to allow asynchronous handler
})
