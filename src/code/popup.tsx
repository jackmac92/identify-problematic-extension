import React, { useState } from 'react'
import ReactAce from 'react-ace'
import { render } from 'react-dom'
import { useForm } from 'react-hook-form'
import 'ace-builds/src-noconflict/mode-javascript'
import 'ace-builds/src-noconflict/theme-terminal'
import { browser } from 'webextension-polyfill'
import { getActiveTab } from 'browser-ext-utilz'

async function testCodeSample(tab, payload) {
  const result = await browser.tabs.executeScript(tab.id, {
    code: payload,
  })
  return Boolean(result[0])
}

function respond(targetTab, checkType, payload) {
  browser.runtime.sendMessage({
    type: 'initiateTest',
    message: checkType,
    targetTab,
    payload,
  })
}

const CodeEditor = ({ handleAceText, ...props }) => {
  return (
    <ReactAce
      mode="javascript"
      theme="terminal"
      onChange={(value: string) => handleAceText(value)}
      style={{ height: '300px' }}
      {...props}
    />
  )
}

function Popup({ targetTab }) {
  const [userCode, setUserCode] = useState('')
  const { register, handleSubmit, watch, errors, clearErrors } = useForm()
  const currentCheckType = watch('check-type', 'Manual')
  const onSubmit = async (data) => {
    await respond(targetTab, data['check-type'].toLowerCase(), data.codeinput)
    window.close()
  }
  const onError = (errs) =>
    console.error('Attempted to submit with errors', errs)
  return (
    <form onSubmit={handleSubmit(onSubmit, onError)}>
      <header>
        How would you like to find the problem? Manually or with a code snippet?
      </header>
      <label>
        Interactive manual check
        <input
          type="radio"
          name="check-type"
          value="Manual"
          defaultChecked={currentCheckType === 'Manual'}
          ref={register}
        />
      </label>
      <label>
        Automate checking via script
        <input
          type="radio"
          name="check-type"
          value="Automated"
          defaultChecked={currentCheckType === 'Automated'}
          ref={register}
        />
      </label>
      {currentCheckType === 'Manual' && <h4>Cool</h4>}
      {currentCheckType === 'Automated' && (
        <div id="codePrompt">
          <span style={{ fontWeight: 'bold', color: 'red' }}>
            {errors?.codeinput?.message}
          </span>
          <CodeEditor
            width="375px"
            height="150px"
            handleAceText={(code: string) => {
              setUserCode(code)
              clearErrors('codeinput')
            }}
          />
          <textarea
            style={{ display: 'none' }}
            name="codeinput"
            value={userCode}
            ref={register({
              validate: (value) =>
                testCodeSample(targetTab, value).then(
                  (res) =>
                    res ||
                    'This code sample does not indicate the problem, write a statement which returns `true` when the problem is occurring'
                ),
            })}
          />
        </div>
      )}
      <input type="submit" />
    </form>
  )
}

getActiveTab().then((tab) => {
  render(<Popup targetTab={tab} />, document.getElementById('popup-root'))
})
