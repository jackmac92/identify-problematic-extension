import type { Management, Tabs, Windows } from '@types/webextension-polyfill'
import {
  disableExtensions,
  enableExtensions,
  listExtensions,
  reloadTab,
  createSidekickWindow,
  createContextMenu,
  localStorageAtom,
} from 'browser-ext-utilz'
import { JsonObject } from 'type-fest'
import { browser, Management, Tabs, Windows } from 'webextension-polyfill'

import { deduceProblemExtension } from './deduceProblemExtension'

const PREVIOUS_USER_CONFIGS_KEY = 'prev-ext-enabled-status'

export type WebExtInfo = Pick<
  Management.ExtensionInfo,
  'id' | 'name' | 'enabled'
>

interface ExtEnabledStatus extends JsonObject {
  isEnabled: boolean
  extId: string
}

const lastExtensionStatusMap = localStorageAtom<ExtEnabledStatus[]>(
  PREVIOUS_USER_CONFIGS_KEY
)

const enableExtensionsAndVerify = async (ext) => enableExtensions([ext])
const disableExtensionsAndVerify = async (ext) => disableExtensions([ext])
const beSureExtensionsDisabled = async (disabledExtensions: WebExtInfo[]) =>
  disableExtensions(disabledExtensions as Management.ExtensionInfo[])

const additiveApproach = async <T>(
  extList: T[],
  errorStillOccurring: (disabled: T[]) => Promise<boolean>,
  extensionOff: (a: T) => unknown,
  extensionOn: (a: T) => unknown
): Promise<T[] | null> => {
  await Promise.all(extList.map((a) => extensionOff(a)))
  if (await errorStillOccurring(extList)) {
    return null
  }
  const problemExtensions = []
  for (const ext of extList) {
    await extensionOff(ext)
    if (!(await errorStillOccurring([...problemExtensions, ext]))) {
      problemExtensions.push(ext)
      await Promise.all(extList.map((a) => extensionOn(a)))
      await Promise.all(problemExtensions.map((a) => extensionOff(a)))
      if (!(await errorStillOccurring(problemExtensions))) {
        break
      }
    }
  }
  return problemExtensions
}

export const findProblemExtension = async (
  extList: WebExtInfo[],
  errorStillOccurring: (disabled: WebExtInfo[]) => Promise<boolean>,
  enableExt,
  disableExt
): Promise<WebExtInfo[] | null> => {
  let result = await deduceProblemExtension(
    extList,
    errorStillOccurring,
    enableExt,
    disableExt
  )
  if (result === null) {
    result = await additiveApproach(
      extList,
      errorStillOccurring,
      enableExt,
      disableExt
    )
  }
  return result
}

type symptomCheckCb = (
  disabledExtensions: WebExtInfo[],
  {
    targetTabId,
    winMessage,
  }: {
    targetTabId: number
    winMessage: <T>(a: unknown) => Promise<T>
  }
) => Promise<boolean> | boolean

class SidekickHandler {
  windowReady = false
  win: Windows.Window = null
  winTab: Tabs.Tab = null
  // @ts-expect-error browser API types are incomplete
  sideKickWinClosedListener: ((windowId: number) => void) | null = null
  createOnDemand = async () => {
    this.win = await createSidekickWindow(
      browser.runtime.getURL('/sideViewPrompt/index.html')
    )
    this.sideKickWinClosedListener = (windowId) => {
      if (windowId === this.win.id) {
        throw new Error('The window was closed!')
      }
    }
    browser.windows.onRemoved.addListener(this.sideKickWinClosedListener)
    const fullWinInfo = await browser.windows.get(this.win.id, {
      populate: true,
    })

    this.winTab = (fullWinInfo.tabs as Tabs.Tab[])[0]
    this.windowReady = true
  }

  sendMessage = async (opts) => {
    if (!this.windowReady) {
      await this.createOnDemand()
    }
    return browser.tabs.sendMessage(this.winTab.id, opts)
  }

  close = async () => {
    browser.windows.onRemoved.removeListener(this.sideKickWinClosedListener)
    return browser.windows.remove(this.win.id)
  }
}

async function genSidekickWinTab() {
  const winHandler = new SidekickHandler()
  return { winMessage: winHandler.sendMessage, winCleanup: winHandler.close }
}

async function findTheTroublemaker(
  tab: Tabs.Tab,
  functionToCheckForProblem: symptomCheckCb
) {
  const allExts = await listExtensions()

  // Allowed debug logging
  console.debug('Got extension list', allExts)
  const { winMessage, winCleanup } = await genSidekickWinTabIdle()
  const originalExtStatus: ExtEnabledStatus[] = allExts.map((ext) => ({
    extId: ext.id,
    isEnabled: ext.enabled,
  }))

  await lastExtensionStatusMap.set(originalExtStatus)

  try {
    if (typeof tab.id !== 'number') {
      throw new TypeError(`Couldn't get id for active tab`)
    }

    let timeout: number

    const resetTimeout = () => {
      // @ts-ignore
      if (timeout) clearTimeout(timeout)
      timeout = window.setTimeout(
        () => {
          throw new Error(
            'Timed out looking for problem ext, reverting changes'
          )
        },
        1000 * 60 * 5
      )
    }
    const testIfIssuePersistsCb = (disabledExtensions: WebExtInfo[]) =>
      beSureExtensionsDisabled(disabledExtensions).then(() =>
        reloadTab(tab.id)
          .then(() => resetTimeout())
          .then(() => {
            const result = functionToCheckForProblem(disabledExtensions, {
              targetTabId: tab.id,
              winMessage,
            })
            if (result === null) {
              throw new Error('User cancelled')
            }
            return Promise.resolve(result)
          })
          .catch((err) => {
            console.error('functionTocheckforprob encountered an error', err)
            throw err
          })
      )

    const problemExt = await findProblemExtension(
      allExts,
      testIfIssuePersistsCb,
      enableExtensionsAndVerify,
      disableExtensionsAndVerify
    )

    if (problemExt === null || problemExt.length === 0) {
      await winMessage({
        type: 'alert',
        message: `Unable to figure out which extension is causing issues, likely because more than one extension causes the problem`,
      })
    } else if (problemExt.length > 1) {
      await winMessage({
        type: 'alert',
        message: `Multiple extensions were causing the problem!\n\n${problemExt.join(
          '\n'
        )}`,
      })
    } else {
      const { value: shouldDisableProblemExt } = await winMessage({
        type: 'binaryResponse',
        message: `Great! it seems the problem was with ${problemExt[0].name}, wanna disable it?`,
      })
      console.debug(
        `Should disable problem extension?: ${shouldDisableProblemExt}`
      )
      if (shouldDisableProblemExt) {
        console.debug('Disabling problem extension')
        const problemExtRef = originalExtStatus.find(
          (ext) => ext.extId === problemExt[0].id
        )
        if (problemExtRef) {
          problemExtRef.isEnabled = false
        }
      }
    }
  } catch (err) {
    console.log('Encountered an error!')
    console.error(err)
  } finally {
    console.debug('Wrapping things up!')
    winMessage({
      type: 'alert',
      message: 'Just a few seconds while I tidy up after myself',
    })
    await Promise.all(
      originalExtStatus.map(({ extId, isEnabled }) =>
        browser.management.setEnabled(extId, isEnabled)
      )
    )
    console.debug('Extension statuses restored')
    await winCleanup()
    if (typeof tab.id === 'number') {
      await reloadTab(tab.id)
    }
  }
}

createContextMenu(
  'Set extensions to their previous state',
  async () => {
    const originalExtStatus = await lastExtensionStatusMap.get()
    await Promise.all(
      originalExtStatus.map(({ extId, isEnabled }) =>
        browser.management.setEnabled(extId, isEnabled)
      )
    )
  },
  // @ts-expect-error
  { singleUse: true }
)

browser.runtime.onMessage.addListener((req) => {
  const automatedCheckForIssue =
    (userDefinedFunc: string): symptomCheckCb =>
    (_disabledExtensions, { targetTabId }): Promise<boolean> =>
      browser.tabs
        .executeScript(targetTabId, { code: userDefinedFunc })
        .then(([result]) => result)

  const manualCheckForIssue: symptomCheckCb = (
    disabledExtensions,
    { winMessage }
  ): Promise<boolean> => {
    return new Promise((resolve) => {
      winMessage({
        type: 'binaryResponse',
        message: 'Is the error still occuring?',
        payload: `Currently these are disabled:\n${disabledExtensions
          .map((e) => e.name)
          .join('\n')}`,
      })
        // @ts-expect-error
        .then((answer) => resolve(answer.value))
    })
  }
  if (req.type === 'initiateTest') {
    const cb =
      req.message === 'manual'
        ? manualCheckForIssue
        : automatedCheckForIssue(req.payload)

    findTheTroublemaker(req.targetTab, cb)
  }
})
