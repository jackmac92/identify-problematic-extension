import { chunk } from 'lodash'

export const deduceProblemExtension = async <T>(
  extList: T[],
  errorStillOccurring: (disabled: T[]) => Promise<boolean>,
  extensionsOn: (a: T) => Promise<unknown>,
  extensionsOff: (a: T) => Promise<unknown>,
  dismissedExtList: T[] = []
): Promise<T[] | null> => {
  if (extList.length === 1) {
    await extensionsOff(extList[0])
    if (await errorStillOccurring([extList[0]])) {
      return null
    }
    return [extList[0]]
  }
  const halfN = Math.ceil(extList.length / 2)
  const [extensionsToDisable, remainingExtensions] = chunk(extList, halfN)

  await Promise.all(extensionsToDisable.map((a) => extensionsOff(a)))

  let nextCandidates: T[]
  let dismissedSuspects: T[]
  if (await errorStillOccurring(extensionsToDisable)) {
    // error is still occuring with this group off -> check other group next
    nextCandidates = remainingExtensions
    dismissedSuspects = extensionsToDisable
  } else {
    // error is not occurring with these turned off -> narrow down
    nextCandidates = extensionsToDisable
    dismissedSuspects = remainingExtensions
    await Promise.all(extensionsToDisable.map((a) => extensionsOn(a)))
  }
  return deduceProblemExtension(
    nextCandidates,
    errorStillOccurring,
    extensionsOff,
    extensionsOn,
    [...dismissedExtList, ...dismissedSuspects]
  )
}

export const deduceProblemExtensionDumb = async <T>(
  extList: T[],
  errorStillOccurring: () => Promise<boolean>,
  _extensionsOn: (a: T) => Promise<unknown>,
  extensionsOff: (a: T) => Promise<unknown>
): Promise<T[] | null> => {
  for (let i = 0; i < extList.length; ++i) {
    await extensionsOff(extList[i])
    if (!(await errorStillOccurring())) {
      return [extList[i]]
    }
  }
  return null
}
