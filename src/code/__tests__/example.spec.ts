import * as fc from 'fast-check'

import type { WebExtInfo } from '../background'
import {
  deduceProblemExtension,
  deduceProblemExtensionDumb,
} from '../background/deduceProblemExtension'

interface WebExtTest extends WebExtInfo {
  isBroken?: boolean
}

describe('this project', () => {
  it('handles a known case by brute force', async () => {
    const littlefucker: WebExtTest = {
      isBroken: true,
      enabled: true,
      id: 'target',
      name: 'littlefucker',
    }
    const extList: WebExtTest[] = [
      littlefucker,
      ...new Array(10).fill(0).map(
        (_, i) =>
          ({
            isBroken: false,
            id: `${i}`,
            enabled: true,
            name: `test-${i}`,
          }) as WebExtTest
      ),
    ]
    const extIsEnabled: Record<string, boolean> = {}
    extList.forEach((e: WebExtTest) => {
      extIsEnabled[e.id] = true
    })
    extList.forEach((e: WebExtTest) => {
      extIsEnabled[e.id] = true
    })
    const checkErrorOccuring = () => {
      const result = extList.some(
        (e) => extIsEnabled[`${e.name}`] && e.isBroken
      )
      return Promise.resolve(result)
    }
    const enableExt = (e: WebExtTest) => {
      extIsEnabled[e.id] = true
      return Promise.resolve()
    }
    const disableExt = (e: WebExtTest) => {
      extIsEnabled[e.id] = false
      return Promise.resolve()
    }
    const res = await deduceProblemExtensionDumb<WebExtTest>(
      extList as unknown as WebExtTest[],
      checkErrorOccuring,
      enableExt,
      disableExt
    )
    expect(res.length).toBe(1)
    expect(res[0]).toBe(littlefucker)
  })
  it('handles a known case smartly', async () => {
    const littlefucker: WebExtTest = {
      isBroken: true,
      enabled: true,
      id: 'target',
      name: 'littlefucker',
    }
    const extList: WebExtTest[] = [
      littlefucker,
      ...new Array(10).fill(0).map((_, i) => ({
        isBroken: false,
        id: `${i}`,
        enabled: true,
        name: `test-${i}`,
      })),
    ]
    const extIsEnabled = {}
    extList.forEach((e) => {
      extIsEnabled[e.id] = true
    })
    const checkErrorOccuring = () => {
      const result = extList.some(
        (e) => extIsEnabled[`${e.name}`] && e.isBroken
      )
      return Promise.resolve(result)
    }
    const enableExt = (e: WebExtTest) => {
      extIsEnabled[e.id] = true
      return Promise.resolve()
    }
    const disableExt = (e: WebExtTest) => {
      extIsEnabled[e.id] = false
      return Promise.resolve()
    }
    const res = await deduceProblemExtension<WebExtTest>(
      extList as unknown as WebExtTest[],
      checkErrorOccuring,
      enableExt,
      disableExt
    )
    expect(res.length).toBe(1)
    expect(res[0]).toBe(littlefucker)
  })

  // TODO: Re-enable once type issues are resolved
  it.skip('handles no problems', () => {
    const checkErrorOccuring = () => {
      return Promise.resolve(false)
    }
    const enableExt = (e: WebExtTest) => {
      e.enabled = true
      return Promise.resolve()
    }
    const disableExt = (e: WebExtTest) => {
      e.enabled = false
      return Promise.resolve()
    }
    fc.assert(
      fc.property(
        fc.array(
          fc.record({
            id: fc.uuid({ version: 4 }),
            name: fc.string(),
            enabled: fc.boolean(),
            isBroken: fc.constantFrom(false),
          })
        ),
        (extList) => {
          expect(
            deduceProblemExtensionDumb(
              extList as unknown as WebExtTest[],
              checkErrorOccuring,
              enableExt,
              disableExt
            )
          ).toBe(null)
        }
      )
    )
  })
})
